'use strict';


/*  -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------


    * * * * * * * * * * * * * * * *
    *  1. C A L C U L A D O R A   *
    * * * * * * * * * * * * * * * *

    Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones.

        - El programa debe recibir dos nÃºmeros (n1, n2).

        - Debe existir una variable que permita seleccionar de alguna forma el tipo de operaciÃ³n (suma, resta, multiplicaciÃ³n
          o divisiÃ³n).

        - Opcional: agrega una operaciÃ³n que permita elevar n1 a la potencia n2.


    ----------------------------------------------------------------------------------------------------------------------- */
//NUMBER 1//
let number1 = parseInt(prompt('C A L C U L A T O R\nEnter the first number:'));;

while (Number.isNaN(number1)) {
    number1 = parseInt(prompt('C A L C U L A T O R\nEnter the first number:', 'WRONG INPUT - TRY AGAIN'));
}

//OPERATOR//
let operator = prompt('C A L C U L A T O R\nEnter the operator:\n· addition: +\n· subtraction: -\n· division: /\n· multiplication: *\n· power: pow');

while (operator !== 'pow' && operator !== '+' && operator !== '-' && operator !== '*' && operator !== '/') {
    operator = prompt('C A L C U L A T O R\nEnter the operator:\n· addition: +\n· subtraction: -\n· division: /\n· multiplication: *\n· power: pow\n', 'WRONG INPUT - TRY AGAIN');
}

//NUMBER 2//
let number2 = parseInt(prompt('C A L C U L A T O R\nEnter the second number:'));

while (Number.isNaN(number2)) {
    number2 = parseInt(prompt('C A L C U L A T O R\nEnter the second number:', 'WRONG INPUT - TRY AGAIN'));
}

//FUNCTION//
function calculator(num1, num2) {
    switch (operator) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '/':
            return num1 / num2;
        case '*':
            return num1 * num2;
        case 'pow':
            return num1 ** num2;
    }
}

console.log('C A L C U L A T O R');
console.log('Result:', number1, operator, number2, '=', calculator(number1, number2));
/*-----------------------------------------------------------------------------------------------------------------------


* * * * * * * * * * * * * * * * * * * * *
*  2. D A D O   E L E C T R Ã“ N I C O   *
* * * * * * * * * * * * * * * * * * * * *

Simula el uso de un dado electrÃ³nico cuyos valores al azar irÃ¡n del 1 al 6.

- Crea una variable "totalScore" en la que se irÃ¡ almacenando la puntuaciÃ³n total tras cada una de las tiradas.

- Una vez alcanzados los 50 puntos el programa se detendrÃ¡ y se mostrarÃ¡ un mensaje que indique el fin de la partida.

- Debes mostrar por pantalla los distintos valores que nos devuelva el dado (nÃºmeros del 1 al 6) asÃ­ como el valor de la
variable "totalScore" tras cada una de las tiradas.


----------------------------------------------------------------------------------------------------------------------- */
class Dice {
    value;

    constructor() {
        this.value = 0;
    }

    throwDice() {
        const randomValueGenerated = Math.round(Math.random(6 - 1) * 6);
        console.log('throwing the dice...');
        this.value = randomValueGenerated;
    }
}

class Game {
    MAX_SCORE = 50;

    totalScore = 0;
    totalNumOfThrows = 0;

    dice;

    constructor() {
        this.dice = new Dice();

        this.initialGameStatus();
    }

    initialGameStatus() {
        console.log('-'.repeat(25));
        console.log('W E L C O M E   T O   T H E   "D I C E   G A M E"');
        console.log(`RULES: You need to throw the dice till you reach ${this.MAX_SCORE}`);
        console.log('-'.repeat(25));
        console.log('Initial status:');
        console.log('totalScore:', this.totalScore);
        console.log('-'.repeat(25));
    }

    play() {
        while (this.totalScore <= this.MAX_SCORE) {
            this.dice.throwDice();
            this.totalNumOfThrows++;

            console.log('Dice Value:', this.dice.value);

            this.totalScore += this.dice.value;
            console.log('totalScore:', this.totalScore);

        }
        this.gameOverStatus();

    }

    gameOverStatus() {
        console.log('-'.repeat(25));
        console.log('G A M E   O V E R ');
        console.log(`WOW! You reached ${this.MAX_SCORE} with ${this.totalNumOfThrows} dice throws!!\nCongrats!`);
        console.log('-'.repeat(25));
    }
}

const game = new Game();
game.play();

/*-----------------------------------------------------------------------------------------------------------------------


* * * * * * * * * * * * * * * * * * * * * * *
*  3. R E G I S T R O   A C A D Ã‰ M I C O   *
* * * * * * * * * * * * * * * * * * * * * * *

En este Ãºltimo ejercicio vamos a asignar una serie de alumnos a distintos profesores.

    - Crea la clase "Person" que incluya las propiedades "name", "age" y "gender", y un mÃ©todo que muestre por pantalla las
      propiedades de una persona.

    - Crea la clase "Student". Esta clase debe heredar de "Person", y debe incluÃ­r, a mayores, las propiedades: "subject"
      y "group", y un mÃ©todo que permita registrar una lista de estudiantes.

    - Crea la clase "Teacher". Esta clase debe heredar de "Person", y debe incluÃ­r, a mayores, las propiedades: "subject", "group"
      y "studentsList". Esta Ãºltima propiedad serÃ¡ un array. Crea un mÃ©todo que permita asignar alumnos al profesor. Esta asignaciÃ³n
      se almacenarÃ¡ en "studentList". Solo los alumnos cuya propiedad "subject" y "group" coincida con la del profesor serÃ¡n almace-
      nados en la propiedad "studentList" de cada profesor.

    - El objetivo final es mostrar por pantalla la lista de profesores junto a todas sus propiedades, entre ellas, los alu-
      mnos que tiene asignados.

    - Deben figurar un mÃ­nimo de tres profesores.


-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
*/

class Person {
    name;
    age;
    gender;

    /**
     * Constructor
     * @param {string} name 
     * @param {number} age 
     * @param {string} gender 
     */
    constructor(name, age, gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    /**
     * This method prints a bio of the person with the values of all attributes.
     */
    printBio() {
        console.log('-'.repeat(10), 'B I O', '-'.repeat(10));
        console.log('Name:', name);
        console.log('Age:', age);
        console.log('Gender:', gender);
        console.log('-'.repeat(28));
    }
}

class SchoolProfile extends Person {
    subject;
    group;

    /**
     * Constructor
     * @param {string} name 
     * @param {number} age 
     * @param {string} gender 
     * @param {string} subject 
     * @param {number} group 
     */
    constructor(name, age, gender, subject, group) {
        super(name, age, gender);
        this.subject = subject;
        this.group = group;
    }
}

class Student extends SchoolProfile {

    /**
     * Constructor
     * @param {string} name
     * @param {number} age
     * @param {string} gender
     * @param {string} subject
     * @param {number} group
     */
    constructor(name, age, gender, subject, group) {
        super(name, age, gender, subject, group);
    }

    //STATIC METHODS//
    /**
     * This method creates a student list.
     * @param {string[]} schoolSubjectsArray Subjects available at school
     * @param {number} numberOfStudents 
     */
    static async createStudentList(schoolSubjectsArray, numberOfStudents) {
        let studentsList = [];

        for (let i = 0; i < numberOfStudents; i++) {
            let newStudent = await this.generateRandomStudent(schoolSubjectsArray);
            studentsList.push(newStudent);
        }

        return studentsList;
    }

    /**
     * This method generates a new random Student.
     * @param {string[]} schoolSubjectsArray Subjects available at school
     */
    static async generateRandomStudent(schoolSubjectsArray) {
        //random age, group and subject//
        const age = this.randomIntInBounds(12, 19);
        const group = this.randomIntInBounds(1, 4);
        const subject = schoolSubjectsArray[this.randomIntInBounds(0, schoolSubjectsArray.length)];

        //random name and gender//
        let response = await fetch('https://randomuser.me/api/');
        let json = await response.json();
        let data = await json.results[0];

        return new Student(data.name.first, age, data.gender, subject, group);
    }

    //SOME EXTRA METHODS//
    /**
     * This method generates a random Integer between min (included) and max (excluded).
     * @param {number} min (included)
     * @param {number} max (excluded)
     */
    static randomIntInBounds(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}

class Teacher extends SchoolProfile {
    #studentList = [];

    /**
     * Constructor
     * @param {string} name
     * @param {number} age
     * @param {boolean} gender
     * @param {string} subject
     * @param {number} group
     */
    constructor(name, age, gender, subject, group) {
        super(name, age, gender, subject, group);
    }

    //METHODS//

    /**
     * This method maps the list of students given as parameter and adds to the #studentList of the teacher
     * those students that match in 'group' and 'subject' with the teacher's group and subject.
     * @param {Student[]} listOfStudents 
     */
    assignStudents(listOfStudents) {
        listOfStudents.map((student) => {
            if (this.subject === student.subject && this.group === student.group) {
                this.#studentList.push(student);
            }
        });
    }

    /**
     * This method returns the private attribute #studentList's value.
     */
    get studentList() {
        return this.#studentList;
    }

    //OTHER METHODS//
    /**
     * This static method prints all teachers' #studentList.
     * @param {Teacher[]} arrayOfTeachers 
     */
    static printAllTeachersList(arrayOfTeachers) {
        console.log('-'.repeat(50), 'TEACHERS LISTS', '-'.repeat(60));
        arrayOfTeachers.map((teacher) => {
            console.log(teacher, '\nTeacher student List:', teacher.studentList);
        });
    }

}

///////////////////////////////////////////////////////////////////

const schoolSubjects = ['Science', 'Mathematics', 'English', 'Spanish', 'Phisical Education'];
const MAX_STUDENTS = 20;

const scienceTeacher = new Teacher('Victoria', 50, 'female', 'Science', 1);
const mathTeacher = new Teacher('Rosana', 38, 'female', 'Mathematics', 2);
const peTeacher = new Teacher('Jose', 25, 'male', 'Phisical Education', 1);

// TEACHERS //
const teachers = [scienceTeacher, mathTeacher, peTeacher];

console.log('All teachers:', teachers);

// STUDENTS //
const studentsList = Student.createStudentList(schoolSubjects, MAX_STUDENTS).then((studentsList) => {
    console.log('All students list:', studentsList);
    return studentsList;
});

async function assignStudentsToTeachers(arrayOfTeachers, arrayOfStudents) {
    const listOfStudents = await arrayOfStudents;

    for (let teacher of arrayOfTeachers) {
        await teacher.assignStudents(listOfStudents);
    }

    Teacher.printAllTeachersList(arrayOfTeachers);
}

assignStudentsToTeachers(teachers, studentsList);


